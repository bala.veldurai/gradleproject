package week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcLogin {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("userRegistrationForm:userName").sendKeys("test1");
		driver.findElementById("userRegistrationForm:password").sendKeys("password");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("password");
		WebElement ques=driver.findElementById("userRegistrationForm:securityQ");
		Select security=new Select(ques);
		security.selectByIndex(2);
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("hero");
		driver.findElementById("userRegistrationForm:prelan");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("firstname");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("midname");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("lastname");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementByName("userRegistrationForm:maritalStatus").click();
		WebElement ele1=driver.findElementById("userRegistrationForm:occupation");
		Select occupation=new Select(ele1);
		occupation.selectByVisibleText("Private");
		driver.findElementById("userRegistrationForm:uidno").sendKeys("369852147852");
		driver.findElementById("userRegistrationForm:idno").sendKeys("asdf2589wf6");
		WebElement ele2=driver.findElementById("userRegistrationForm:countries");
		Select country=new Select(ele2);
		country.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("test@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("+91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("1472589654");
		WebElement ele3=driver.findElementById("userRegistrationForm:nationalityId");
		Select nationality=new Select(ele3);
		nationality.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("123,omr road");
		driver.findElementById("userRegistrationForm:street").sendKeys("streetname");
		driver.findElementById("userRegistrationForm:area").sendKeys("areaname");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("603103",Keys.TAB);
		Thread.sleep(1000);
		WebElement ele4=driver.findElementById("userRegistrationForm:cityName");
		Select town=new Select(ele4);
		town.selectByVisibleText("Kanchipuram");
		Thread.sleep(1000);
		WebElement ele5=driver.findElementById("userRegistrationForm:postofficeName");
		Select PO=new Select(ele5);
		PO.selectByValue("Navalur B.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("2589654125");		

	}

}
