package testCasesTestngExecution;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC002_CreateLead extends ProjectMethods {

	@BeforeTest(groups= {"smoke"})
	public void setData() {
		testCaseName="TC002_CreateLead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="Bala";
		Data="createLead";
	}

	@Test(groups= {"smoke"}, dataProvider="testData")

	public void createLead(String companyName, String fName, String lName) {

		WebElement linktext = locateElement("linktext", "Create Lead");
		click(linktext);
		WebElement cmpyName = locateElement("id", "createLeadForm_companyName");
		type(cmpyName, companyName);
		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		type(lastName, fName);
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, lName);
		/*WebElement nameLocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(nameLocal, "Test1");
		WebElement lastLocal = locateElement("id", "createLeadForm_lastNameLocal");
		type(lastLocal, "abc");
		WebElement salutation = locateElement("id", "createLeadForm_personalTitle");
		type(salutation, "Mr");
		WebElement source = locateElement("createLeadForm_dataSourceId");
		selectDropDownUsingIndex(source, 4);
		WebElement title = locateElement("id", "createLeadForm_generalProfTitle");
		type(title, "sampleTest");
		WebElement revenue = locateElement("createLeadForm_annualRevenue");
		type(revenue, "100000");
		WebElement country = locateElement("createLeadForm_industryEnumId");
		selectDropDownUsingText(country, "Health Care");
		WebElement ownerShip = locateElement("createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ownerShip, "Corporation");
		WebElement ele1 = locateElement("createLeadForm_sicCode");
		type(ele1, "603103");
		WebElement ele2 =locateElement("createLeadForm_description");
		type(ele2, "TestLeaf");
		WebElement note = locateElement("createLeadForm_importantNote");
		type(note, "Sample Notes");
		WebElement countryCode = locateElement("createLeadForm_primaryPhoneCountryCode");
		type(countryCode, "603103");
		WebElement phoneArea = locateElement("createLeadForm_primaryPhoneAreaCode");
		type(phoneArea, "044");
		WebElement phExten = locateElement("createLeadForm_primaryPhoneExtension");
		type(phExten, "044");
		WebElement departName = locateElement("createLeadForm_departmentName");
		type(departName, "IT");
		WebElement ele4 = locateElement("createLeadForm_currencyUomId");
		selectDropDownUsingText(ele4, "BRR - Brazil");
		WebElement ele5 = locateElement("createLeadForm_numberEmployees");
		type(ele5, "1000");
		WebElement symbol = locateElement("createLeadForm_tickerSymbol");
		type(symbol, "dollar");
		WebElement primaryPhone = locateElement("createLeadForm_primaryPhoneAskForName");
		type(primaryPhone, "9000090000");
		WebElement primaryUrl = locateElement("createLeadForm_primaryWebUrl");
		type(primaryUrl, "https://www.google.com/");
		WebElement toName = locateElement("createLeadForm_generalToName");
		type(toName, "SampleTest");
		WebElement add1 = locateElement("createLeadForm_generalAddress1");
		type(add1, "OMR, Chennai");
		WebElement Add2 = locateElement("createLeadForm_generalAddress2");
		type(Add2, "Kanchipuram");
		WebElement city = locateElement("createLeadForm_generalCity");
		type(city, "Chennai");
		WebElement province = locateElement("createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(province, "Arkansas");
		WebElement geoId = locateElement("createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(geoId, "Algeria");
		WebElement code = locateElement("createLeadForm_generalPostalCodeExt");
		type(code, "603103");
		WebElement codeGen = locateElement("createLeadForm_generalPostalCode");
		type(codeGen, "603103");
		WebElement market = locateElement("createLeadForm_marketingCampaignId");
		selectDropDownUsingText(market, "Car and Driver");
		WebElement priPhone = locateElement("createLeadForm_primaryPhoneNumber");
		type(priPhone, "1000054645");
		WebElement priEmail = locateElement("createLeadForm_primaryEmail");
		type(priEmail, "test@gmail.com");*/
		WebElement submit = locateElement("name", "submitButton");
		click(submit);
		/*WebElement firstNameGet = locateElement("viewLead_firstName_sp");
		verifyExactText(firstNameGet, "test");*/

		//System.out.println("First name is "+getText(firstNameGet));
		//closeBrowser();


	}
	
}
