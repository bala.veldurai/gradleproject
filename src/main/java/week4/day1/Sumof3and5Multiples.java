package week4.day1;

public class Sumof3and5Multiples {

	public static void main(String[] args) {

		int sum=0;
		System.out.println("The series are ");
		for(int i=3;i<100;i++)
		{
			if(i%3==0 || i%5==0) {
				sum=sum+i;
				System.out.print("+"+i);
			}

		}
		System.out.println(" ");

		System.out.println("Sum is "+sum);
	}
}
