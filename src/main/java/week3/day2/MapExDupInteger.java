package week3.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapExDupInteger {

	public static void main(String[] args) {

		int[] a= {13,65,15,67,88,65,13,99,67,13,65,87,13};
		Map<Integer,Integer> maps=new HashMap<>();
		for (int i = 0;i<a.length;i++) 
		{
			int value=1;
			if(!maps.containsKey(a[i]))
			{
				maps.put(a[i], value);
			}
			else
			{
				value=value+1;
				maps.put(a[i], value);
			}
		}
		System.out.println("Duplicated numbers are: ");
		for (Entry<Integer, Integer> s:maps.entrySet())
		{
			if(s.getValue()>1)
				System.out.println(s.getKey());
		}
	}
}
