package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods{

	public MyHomePage() {
		PageFactory.initElements(driver, this);

	}
	@FindBy(how=How.LINK_TEXT,using="Create Lead")
	WebElement eleLink;
	@When("click on CreateLead")
	public CreateLeadPage clickLeads() {
		click(eleLink);
		return new CreateLeadPage();
	}

	@FindBy(how=How.XPATH,using="//a[text()='Leads']")
	WebElement eleLeadlink;
	
	public MyLeadsPage clickLeadsTab() {
		click(eleLeadlink);
		return new MyLeadsPage();
	}

}
