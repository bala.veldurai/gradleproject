package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.ReadExcel;

public class ProjectMethods extends SeMethods{

	@BeforeSuite(groups= {"common"})
	public void beforeSuite() {
		beginResult();
	}

	@BeforeClass(groups= {"common"})
	public void beforeClass() {
		startTestCase();
	}
	@Parameters({"url", "browser"})
	@BeforeMethod(groups= {"common"})
	public void login(String url, String browser) {
		startApp(browser, url);
		/*WebElement username = locateElement("id", "username");
		type(username, username1);
		WebElement password = locateElement("id", "password");
		type(password, password1);
		WebElement button = locateElement("class", "decorativeSubmit");
		click(button);
		WebElement parlinktext = locateElement("partiallinktext", "CRM/SFA");
		click(parlinktext);*/

	}

	@AfterMethod(groups= {"common"})
	public void closeApp() {
		closeBrowser();
	}

	@AfterSuite(groups= {"common"})
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider (name="testData")
	public Object[][] setValue() throws IOException {

		return ReadExcel.getData(Data);		
	}

}
