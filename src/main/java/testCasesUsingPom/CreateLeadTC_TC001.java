package testCasesUsingPom;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class CreateLeadTC_TC001 extends ProjectMethods{
	
	@BeforeTest()
	public void setData() {
		testCaseName="TC001_CreateLead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="Bala";
		Data="createLead";
	}
	
	@Test (dataProvider="testData")
	public void createLead(String loginName,String password, String Companyname, String firstname,String lasNname, String Companyname1)
	{
		new LoginPage().typeUserName(loginName).typePassword(password).clickLogin().clickLink().clickLeads().typeCompanyname(Companyname)
		.typeFirstName(firstname).typeLastName(lasNname).clickSubmit().verifyCompanyname(Companyname1);
		
	}

}
