package testCasesTestngExecution;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC006_DuplicateLead extends ProjectMethods{
	
	@BeforeTest(groups= {"regression"})
	public void setData() {
		testCaseName="TC006_DuplicateLead";
		testCaseDesc="Duplicate a Lead";
		category="smoke";
		author="Bala";
	}

@Test(groups= {"regression"})
	public void dupLead() throws InterruptedException {
		/*startApp("firefox", "http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement username = locateElement("id", "username");
		type(username, "DemoSalesManager");
		WebElement password = locateElement("id", "password");
		type(password, "crmsfa");
		WebElement button = locateElement("class", "decorativeSubmit");
		click(button);
		WebElement parlinktext = locateElement("partiallinktext", "CRM/SFA");
		click(parlinktext);*/
		WebElement leads = locateElement("linktext", "Leads");
		click(leads);
		WebElement findLeads = locateElement("linktext", "Find Leads");
		click(findLeads);
		locateElement("xpath", "//span[text()='Phone']").click();
		WebElement locateElement = locateElement("name", "phoneNumber");
		type(locateElement, "100");
		WebElement buttonClick = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonClick);
		
		
		locateElement("xpath", "//span[text()='Email']").click();
		WebElement emailClick = locateElement("xpath", "//input[@name='emailAddress']");
		type(emailClick, "test@gmail.com");
		
		WebElement linkClick = locateElement("xpath","(//a[@class='linktext'])[4]");
		click(linkClick);
		Thread.sleep(1000);
		locateElement("xpath", "//a[text()='Duplicate Lead']").click();
		System.out.println("The page title is "+getTitle());
		//WebElement getName = locateElement("viewLead_firstName_sp");
		//String actual=getText(getName);
		locateElement("class", "smallSubmit").click();
		WebElement getName = locateElement("viewLead_firstName_sp");
		if(getText(getName).equals("Test")) {
			System.out.println("Both lead names are matches");
		}
		//closeBrowser();
	}

}
