package testCasesTestngExecution;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class EditLead extends ProjectMethods{
	@BeforeTest(groups = {"sanity"})
	public void setValues() {
		testCaseName = "TC002_EditLead";
		testCaseDesc =  "Edit Lead";
		category = "sanity";
		author = "Bala";
		Data="editLead";

		
	}
	
	@Test(groups = {"sanity"}, dataProvider="testData")
	public void editLead(String selectDropdown) throws InterruptedException {
		
		click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), "");
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(3000);
		click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		verifyTitle("View Lead | opentaps CRM");
		click(locateElement("xpath", "//a[contains(text(),'Edit')]"));
		selectDropDownUsingText(locateElement("id", "updateLeadForm_industryEnumId"), selectDropdown);
		click(locateElement("xpath", "//input[@class='smallSubmit']"));			
	}
	
	/*@DataProvider(name="posdata")
	public void setDatas() {
		Object[] data=new Object[2];
		data[0]=""
	}*/
}
