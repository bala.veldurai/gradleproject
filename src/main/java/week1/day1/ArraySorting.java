package week1.day1;

import java.util.Scanner;

public class ArraySorting {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the array size");
		int size=input.nextInt();
		System.out.println("Enter the array values");
		int temp;
		int a[]=new int[size];
		for(int i=0; i<size; i++)
		{
			a[i]=input.nextInt();
		}

		for(int i=0; i<size; i++)
		{
			for(int j=i+1; j<size; j++)
			{
				if(a[i]>a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}

		}
		System.out.println("Sorted array is :" );

		for(int i=0; i<size; i++) {
		System.out.println(a[i]);
		}
	}
}
