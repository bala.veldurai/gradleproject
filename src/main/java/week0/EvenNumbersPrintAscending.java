package week0;

import java.util.Scanner;

public class EvenNumbersPrintAscending {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the starting value to print ");
		int value1=input.nextInt();
		System.out.println("Enter the end value to print ");
		int value2=input.nextInt();
		
		for (int i=value1; i<=value2; i=i+2)
		{
			System.out.println(i);
		}

	}

}
