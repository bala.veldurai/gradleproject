package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListPgm {
	
	public static void main(String[] args) {
		
		List<String> mobilePhones=new ArrayList<String>();
		//add elements to arraylist
		mobilePhones.add("Sony");
		mobilePhones.add("Nokia");
		mobilePhones.add("Samsung");
		mobilePhones.add("Redmi");
		mobilePhones.add("iPhone");
		mobilePhones.add(2, "Moto");
		
		int x=mobilePhones.size();
		System.out.println("total mobile phone count is "+x);
		System.out.println("default arraylist is "+mobilePhones);
		Collections.sort(mobilePhones);
		System.out.println("The first mobile is "+mobilePhones.get(0));
		System.out.println("The last mobile is "+mobilePhones.get(x-1));
		for (String eachMobile : mobilePhones) {
			System.out.println(eachMobile);
		}
		System.out.println("Reverse sorting is ");

		Collections.reverse(mobilePhones);
		for (String eachMobile:mobilePhones) {
			System.out.println(eachMobile);
		}
			
	}

}
