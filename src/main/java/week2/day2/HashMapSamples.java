package week2.day2;
import java.util.*;

public class HashMapSamples {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HashMap<Integer,String> smap=new HashMap<Integer,String>();
		smap.put(1,"test1");
		smap.put(2,"test2");
		smap.put(3,"test3");
		smap.put(2,"test4");
		smap.put(null, null);
		smap.put(null,"test5");
		smap.put(5, null);
		smap.put(6, null);
		smap.put(4, null);
		for (Map.Entry<Integer,String> m:smap.entrySet()) {

			System.out.println(m.getKey()+ " "+m.getValue()+" ");			
		}
	}
}
