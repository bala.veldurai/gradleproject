package projectDay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods1;

public class ZoomCar extends SeMethods1{


	@Test
	public void zoomCar() throws InterruptedException {
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement journeyButton = locateElement("xpath", "//a[@title='Start your wonderful journey']");
		click(journeyButton);		
		WebElement pickPoint = locateElement("xpath", "//div[contains(text(),\"Kodambakkam\")]");
		pickPoint.click();
		WebElement buttonClk = locateElement("xpath", "//button[text()='Next']");
		click(buttonClk);
		WebElement date = locateElement("xpath", "//div[text()='Mon']");
		click(date);
		WebElement btnClick = locateElement("xpath", "//button[text()='Next']");
		click(btnClick);
		WebElement doneClick = locateElement("xpath", "//button[text()='Done']");
		click(doneClick);
		List<WebElement> carPrices = driver.findElementsByXPath("//div[@class='car-amount']/following::div[@class='price']");
		List<String> sortOrder=new ArrayList<String>();
		System.out.println("Number of results displayed are "+carPrices.size());
		for (WebElement eachCar : carPrices) {
			System.out.println("Each car Price is "+ eachCar.getText().replaceAll("\\D", ""));
			//eachCar.getText().replaceAll("\\D", "");
			sortOrder.add(eachCar.getText().replaceAll("\\D", ""));
		}
		System.out.println(sortOrder);
		System.out.println("Maximum price is "+Collections.max(sortOrder));
		List<WebElement> carNames = locateElements("xpath", "//h3[text()='Ford Figo']/following::h3");
		int number = sortOrder.indexOf(Collections.max(sortOrder));
		String carBrand = carNames.get(number).getText();
		System.out.println("Car name is "+carBrand);
		List<WebElement> bookNowBtn = locateElements("xpath", "//button[text()='BOOK NOW']");
		bookNowBtn.get(number).click();
		Thread.sleep(1000);
		driver.close();
	}

}
