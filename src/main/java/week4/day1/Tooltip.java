package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class Tooltip extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName="Tooltip";
		testCaseDesc="verify tool tip";
		category="sample";
		author="Bala";
	}

	@Test
	public void toolTip() {

	startApp("chrome", "http://jqueryui.com/tooltip/");
	WebElement locateElement = locateElement("xpath", "//iframe[@class='demo-frame']");
	switchToFrame(locateElement);
	WebElement ageField = locateElement("xpath","//input[@id='age']");
	Actions builder=new Actions(driver);
	builder.moveToElement(ageField).perform();
	
	 String tTip = ageField.getAttribute("title");
	 System.out.println("toop Tip is "+ tTip);
			//equals("We ask for your age only for statistical purposes.");
	/*if(tTip==true) {
	System.out.println("Age tooltip has verified");
	}else
	{
		System.out.println("Age tooltip has not verified");

	}*/
		
	}
	

}
