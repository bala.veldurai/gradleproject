package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods{
	
	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//input[@name='submitButton']")
	WebElement eleSubButton;
	public ViewLeadpage clickCreateLead() {
		click(eleSubButton);
		return new ViewLeadpage();
		
	}
	
	public boolean verifyTitle(String data) {
				
		verifyTitle(data);
		return true;
		
		}

	}
