package testCasesTestngExecution;



import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class DuplicateLead extends ProjectMethods{
	@BeforeTest(groups = {"regression"})
	public void setValues() {
		testCaseName = "TC005_DuplicateLead";
		testCaseDesc =  "Duplicate Lead";
		category = "regression";
		author = "Bala";
		
	}
	@Test(groups = {"regression"})
	public void duplicateLead() throws InterruptedException {
		click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Find Leads"));
		click(locateElement("xpath", "//span[contains(text(),'Phone')]"));
		type(locateElement("name", "phoneNumber"), "");
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(2000);
		String txt = getText(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		click(locateElement("linktext", "Duplicate Lead"));
		click(locateElement("xpath", "//input[@name='submitButton']"));				
	}
}
