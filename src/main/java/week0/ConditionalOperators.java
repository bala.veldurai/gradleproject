package week0;

import java.util.Scanner;


public class ConditionalOperators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		
		Scanner scan= new Scanner(System.in);
		System.out.println("num1 value is ");

		int num1=scan.nextInt();
		System.out.println("num2 value is ");

		int num2=scan.nextInt();

		//Equality and rational operators
		if(num1>num2) 
			System.out.println("Num1 is greater than Num2");
		if (num1==num2)
			System.out.println("Num1 and Num2 are equal");
		if (num1<num2)
			System.out.println("Num1 is less than Num2");
		if(num1!=num2)
			System.out.println("Num1 and Num2 are not equal");
		if(num1>=num2)
			System.out.println("Either numbers are greater or equal");
		if(num1<=num2)
			System.out.println("Either numbers are less or equal");
		System.out.println("Num1 is greater than Num2 " + (num1>num2));


		//conditional operators
		if((num1==5) && (num2 ==6))
			System.out.println("num 1 value is 5 and num2 value is 6");
		
		if((num1==15) || (num2 ==66))
			System.out.println("num 1 value is 15 OR num2 value is 66");
	}

}
