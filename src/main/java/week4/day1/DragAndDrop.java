package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/draggable/");
		WebElement frame = driver.findElementById("draggable");
		driver.switchTo().frame(frame);
		WebElement drag = driver.findElementByXPath("//p[text()='Drag me around']");
		Point location = drag.getLocation();
		System.out.println(location);
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(drag, 37, 53).perform();


	}

}
