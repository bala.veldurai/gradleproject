package week6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PossibleNumberCombinations {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number ");
		int num=sc.nextInt();
		int temp=num;
		List<Integer> array=new ArrayList<Integer>();
		int b;
		while(temp>0) {
			b=temp%10;
			temp=temp/10;
			array.add(b);
		}
		Collections.sort(array);
		System.out.println(array);
		System.out.println("Number combinations are ");
		for (int i=array.size()-1;i>=0;i--) {
			for(int j=array.size()-1;j>=0;j--) {
				for(int k=array.size()-1;k>=0;k--) {
					if(k != i && k != j && i != j) {
						System.out.println(array.get(i)+""+array.get(j)+""+array.get(k));
					}
				}
			}
		}
	}
	
	
	/*void getCombination(int firstDigit, ArrayList<Integer> remainingDigits)
	{
		if(remainingDigits.size()>2) {
		System.out.println(firstDigit);
		ArrayList<Integer> toRecursive = null;
		int temp = remainingDigits.get(0);
		for(int i=0;i<remainingDigits.size();i++) {
			if(remainingDigits.get(i) != temp) {
				toRecursive.add(remainingDigits.get(i));
			}		
		}
		getCombination(toRecuesive.get(0), );
		}
		else {
			System.out.println(remainingDigits.get(0)+""+remainingDigits.get(1));
			System.out.println(remainingDigits.get(1)+""+remainingDigits.get(0));
		}
	}
	*/
	
}
