package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandles {

	public static void main(String[] args) throws InterruptedException, WebDriverException, IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();			
		driver.findElementByLinkText("Contact Us").click();
		String windowHandle = driver.getWindowHandle();
		System.out.println("get window handle is "+windowHandle);
		/*Set<String> windowHandles = driver.getWindowHandles();
		List<String> AllHandles=new ArrayList<String>();
		AllHandles.addAll(windowHandles);
		driver.switchTo().window(AllHandles.get(1));
		System.out.println(driver.getCurrentUrl());
		System.out.println(driver.getTitle());
		WebDriver window = driver.switchTo().window(AllHandles.get(0));
		FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE), new File("./snapshot/img.png"));
		window.close();*/
	}

}
