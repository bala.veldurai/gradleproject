package week1.day1;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.println("Enter the prime number range start");
		int n1 = input.nextInt();
		System.out.println("Enter the prime number range end");
		int n2 = input.nextInt();
		
		//when print number in ascending
		if(n1<n2) 
		{
			PrimeNumber.printPrime(n1,n2);
			


		}
		else if(n1>n2)
		{
			PrimeNumber.printPrime(n2,n1);
			
			}
		
		}
		
	
	public static void printPrime(int initNum, int finNum) {
		int flag=0;  //to check if the particular action is performed
		int count=0;
		for(int i=initNum; i<=finNum; i++ ) 
		{
			for(int j=2; j<i; j++)
			{
				if(i%j==0) 
				{

					flag=0;
					break;
				}

				else 
				{
					flag=1;

				}
			}

			if(flag==1) {
				count++;
				System.out.println(i);
			}
		}
		System.out.println("total prime number count is " + count);

	}
}

