package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class COuntryDropDOWnList {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		WebElement ele=driver.findElementById("userRegistrationForm:countries");
		Select country=new Select(ele);
		List<WebElement> countries=country.getOptions();
		System.out.println("Number of countries are "+countries.size());
		System.out.println("List of countries are ");
		for (WebElement eachCountry : countries) {
			System.out.println(eachCountry.getText());
			if(eachCountry.getText().equals("India")){
				country.selectByVisibleText("India");
			}
		}
	}
}
