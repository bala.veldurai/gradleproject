package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);

	}

	@FindBy(id="updateLeadForm_companyName")
	WebElement eleChangeCompany;
	public EditLeadPage changeCompanyName(String data) {
		type(eleChangeCompany, data);
		return this;
	}

	@FindBy(how=How.XPATH, using="(//input[@type='submit'])")
	WebElement eleUpdateBtn;
	public ViewLeadpage clickUpdate() {
		click(eleUpdateBtn);
		return new ViewLeadpage();
	}

}
