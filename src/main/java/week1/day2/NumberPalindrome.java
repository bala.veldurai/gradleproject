package week1.day2;

import java.util.Scanner;

public class NumberPalindrome {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number ");
		int a=sc.nextInt();
		int temp=a; 
		int sum=0;

		while(a>0)
		{
			int rem=a%10;
			sum=(sum*10)+rem;
			a=a/10;
		}
		if(sum==temp)
		{
			System.out.println("Number is palindrome");
		}
		else
			System.out.println("Number is not palindrome");

	}

}
