package week2.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ArraySort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//arrayListSortAscending();
		arraysAscending();
	}
	public static void arrayListSortAscending() {
		Scanner sc= new Scanner(System.in);
		List<Integer> lst=new ArrayList<>();
		System.out.println("Enter the numbers ");
		for(int i=0;i<7;i++)
		{
			lst.add(sc.nextInt());
		}
		Collections.sort(lst);
		System.out.println("Ascending Order: "+lst);

		Collections.reverse(lst);
		System.out.println("Desending Order: "+lst);

	}

	public static void arraysAscending() {
		Scanner sc= new Scanner(System.in);
		int[] lst=new int[7];
		System.out.println("Enter the numbers ");
		for(int i=0;i<7;i++)
		{
			lst[i]=sc.nextInt();
		}
		
		for(int i=0;i<7;i++)
		{
			for(int j=0;j<7;j++)
			{
				if(lst[i]<lst[j])
				{
					int temp=lst[i];
					lst[i]=lst[j];
					lst[j]=temp;
				}
			}

		}
		System.out.println("Ascending order is: ");
		for(int i=0;i<7;i++)
		{
			System.out.println(lst[i]);
		}
		System.out.println("Decending order is: ");
		for(int i=6;i>=0;i--)
		{
			System.out.println(lst[i]);
		}
		


	}

}
