package week6;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.WebDriverException;
import org.testng.annotations.Test;

public class TestNgOrder {


	@Test
	public void methodA() {
		System.out.println("Method A");
	}

	@Test(dependsOnMethods= {"methodC"})
	public void methodB() {
		System.out.println("Method B");
	}

	@Test(dependsOnMethods= {"methodA"})
	public void methodC() {
		System.out.println("Method C");
	}

	@Test
	public void methodD() {
		System.out.println("Method D");
	}

	public void exceptionMethod() {
		int a=2, b=7 ,c=0;
		try {
			 c=  a+b;
			 System.out.println("sum is "+c);
		}
		catch (NoSuchElementException e) {
			System.err.println(e);
		}catch (ElementNotVisibleException e)
		{
			System.err.println(e);
		}catch (NoSuchSessionException e)
		{
			System.err.println(e);
		}catch (WebDriverException e) {
			System.err.println(e);
			
		}catch (Exception e) {
			System.err.println(e);
		}
	}
}

