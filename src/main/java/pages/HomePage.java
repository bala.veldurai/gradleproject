package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);

	}

	@FindBy(how=How.PARTIAL_LINK_TEXT, using="CRM/SFA")
	WebElement eleLink;
	@When("click on crmsfa link")
	public MyHomePage clickLink() {
		click(eleLink);		
		return new MyHomePage();
	}
	
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit")
	WebElement eleLogin;
	public LoginPage clickLogout() {

		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new LoginPage();
	}
}
