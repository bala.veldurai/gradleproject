package week4.day1;

import java.util.Scanner;

public class LargeNumberLessthanGivenNumber {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number ");
		int number = sc.nextInt();
		System.out.println("Enter a digit ");
		int digit1 = sc.nextInt();
		sc.close();

		char digit = Integer.toString(digit1).charAt(0);

		while (number>0) {

			number--;
			int temp = Integer.toString(number).indexOf(digit);
			if(temp==-1)
			{
				System.out.println("The largest number is "+number);
				break;

			}
		}		
	}

}
