package testCasesTestngExecution;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdMethods.SeMethods;

public class TC004_MergeLead extends SeMethods{

	@Test
	public void mergeLead() {

		/*startApp("edge", "http://leaftaps.com/opentaps/control/main");
		WebElement username = locateElement("id", "username");
		type(username, "DemoSalesManager");
		WebElement password = locateElement("id", "password");
		type(password, "crmsfa");
		WebElement button = locateElement("class", "decorativeSubmit");
		click(button);
		WebElement parlinktext = locateElement("partiallinktext", "CRM/SFA");
		click(parlinktext);*/
		WebElement leads = locateElement("linktext", "Leads");
		click(leads);
		WebElement mergeLeadButton = locateElement("xpath", "//a[text()='Merge Leads']");
		click(mergeLeadButton);
		WebElement locateElement = locateElement("xpath", "//img[@alt='Lookup']");
		click(locateElement);
		switchToWindow(1);
		WebElement locateElement2 = locateElement("name", "id");
		type(locateElement2, "100");
		WebElement findLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeads);	
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@class='linktext'])[4]")));
		switchToWindow(0);
		locateElement("linktext", "Merge").click();
		acceptAlert();
		locateElement("linktext", "Find Leads").click();
		//driver.quit();
		
	}
	
	@DataProvider (name="mergeLead")
	public void data() {
		Object[] element=new Object[2];
		
		element[0]="100";
		element[1]="1000";

		
	}
}
