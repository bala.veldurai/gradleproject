package week3.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ListBox {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/Dropdown.html");
		driver.manage().window().maximize();
		WebElement value=driver.findElementByXPath("//Select[@name='dropdown2']");
		Select dp=new Select(value);
		List<WebElement> value1=dp.getOptions();
		dp.selectByIndex(value1.size()-2);

	}

}
