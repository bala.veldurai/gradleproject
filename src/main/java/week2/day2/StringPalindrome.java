package week2.day2;

public class StringPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		palindromeString("madam");

	}
	
	public static void palindromeString (String name)
	{
		char[] eachChar=name.toCharArray();
		String rev="";
		int len=eachChar.length;
		for(int i=len-1;i>=0;i--)
		{
			rev=rev+eachChar[i];
		}
		if(rev.equals(name))
		{
			System.out.println("String is palindrome ");
		}
		else
			System.out.println("String is not palindrome ");
	}

}
