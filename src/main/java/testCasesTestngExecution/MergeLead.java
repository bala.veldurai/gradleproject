package testCasesTestngExecution;



import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class MergeLead extends ProjectMethods{
	@BeforeTest(groups = {"regression"})
	public void setValues() {
		testCaseName = "TC004_MergeLead";
		testCaseDesc =  "Merge Lead";
		category = "regression";
		author = "gopi";		
	}
	@Test(groups = {"regression"}, dataProvider="posdata")
	public void mergeLead(String fName, String editName) throws InterruptedException {
		click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Merge Leads"));
		click(locateElement("xpath", "(//img[@alt='Lookup'])[1]"));
		switchToWindow(1);
		type(locateElement("name", "firstName"), fName);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(2000);
		String txt = getText(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		switchToWindow(0);
		
		click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));
		switchToWindow(1);
		type(locateElement("name", "firstName"), editName);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(2000);		
		click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		switchToWindow(0);
		
		click(locateElement("linktext", "Merge"));
		acceptAlert();
		
		click(locateElement("linktext", "Find Leads"));
		type(locateElement("xpath", "//label[contains(text(),'Lead ID:')]/following::input"), txt);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		verifyPartialText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display");		
	}
	
	@DataProvider (name="posdata")
	public Object[] setdata() {
		Object[][] data=new Object[2][2];
		data[0][0]="Gopi";
		data[0][1]="Babu";
		
		data[1][0]="test";
		data[1][1]="test";
		
		return data;

	}
}
