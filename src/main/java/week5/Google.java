package week5;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class Google extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName="Google launch";
		testCaseDesc="goolge check";
		category="smoke";
		author="Bala";
	}
	
	@Test
	public void googleLaunch() throws InterruptedException {
		startApp("firefox", "https://www.google.com/");
		locateElement("lst-ib").sendKeys("selenium test",Keys.TAB);
		//type(locateElement, "selenium test",Keys.TAB);
		Thread.sleep(1000);
		WebElement button = locateElement("name", "btnK");
		Thread.sleep(1000);

		click(button);
		
	}

}
