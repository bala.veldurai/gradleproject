package testCasesUsingPom;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class DeleteLeadTC_TC004 extends ProjectMethods{
	
	@BeforeTest()
	public void setData() {
		testCaseName="TC004_DeleteLead";
		testCaseDesc="Delete lead test case";
		category="smoke";
		author="Bala";
		Data="LoginData";
	}
	
	@Test (dataProvider="testData")
	public void duplicateLead(String loginName, String password ) throws InterruptedException {
		new LoginPage().typeUserName(loginName).typePassword(password).clickLogin().clickLink().clickLeadsTab().clickFindLeads().clickFindLeadsButton()
		.clickFirstResult().deleteLeadButton();
	}
}
