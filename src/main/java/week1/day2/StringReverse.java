package week1.day2;

import java.util.Scanner;

public class StringReverse {

	public static void main(String[] args) {
		// TODO Auto-generated method stu b
		StringReverse obj=new StringReverse();
		obj.reverseStringMethod("Balasundari");
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the string ");
		String s=scan.next();
		String reverse = "";
		
		for(int i=(s.length()-1); i>=0; i--)
		{
			reverse=reverse+s.charAt(i);
		}
		
		if(reverse.equals(s))
			System.out.println("String is palindrome");
		else
		{
			System.out.println("String is not palindrome");

		}

	}
	
	public void reverseStringMethod(String name)
	{
		StringBuffer x=new StringBuffer(name);
		System.out.println(x.reverse());
	}

}
