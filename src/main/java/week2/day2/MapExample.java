package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class MapExample {

	public static void main(String[] args) {
		String name="BalasundariB";
		char[] charArray=name.toCharArray();

		Map<Character, Integer> name1=new HashMap<Character, Integer>();
		//System.out.println("Duplicate chars are ");

		for (char c : charArray) {
			if(name1.containsKey(c))
			{
				Integer value=name1.get(c)+1;
				name1.put(c,value);
				//System.out.println(c);
				//System.out.println("String has duplicates");
				//break;
			}
			else
				name1.put(c,1);
		}
		//System.out.println(name1);
		for (Map.Entry<Character, Integer> eachCh: name1.entrySet()) {
			System.out.println(eachCh.getKey()+" "+eachCh.getValue());
		}
	}
}

