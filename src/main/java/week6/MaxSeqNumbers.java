package week6;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class MaxSeqNumbers {

public static void main(String[] args) {
	seqNumbers();
	//System.out.println(seqNumbers());
	
	
}
			public static void seqNumbers() {
				// TODO Auto-generated method stub
				
				int[] array = {10,15,1,2,3,4,5,11,12,13,15,14,16,17};
				List<Integer> tempList = new ArrayList<>(); // prepare temp list for later use
				List<List<Integer>> arrays = new ArrayList<>(); // used to store the sequences
				int lastNum = array[0]; // get the fist number for compasion in loop
				tempList.add(lastNum);
				for (int i = 1; i < array.length; i++) {
					System.out.println(array[i]+" -- "+lastNum);
				    if (array[i]-1 == lastNum) { // check for sequence (e.g fist num was 12,
				      // current number is 13, so 13-1 = 12, 
				      // so it has the sequence), then store the number
				        tempList.add(array[i]); // store it to the temp list
				        lastNum = array[i]; // keep current number for the next
				    } else { // if it has not the sequence, start the new sequence
				        arrays.add(tempList); // fist store the last sequence
				        tempList = new ArrayList(); // clear for the next sequence
				        lastNum = array[i]; // init the lastNumnber
				        tempList.add(lastNum);
				    }
				}
				// now iterate for the longest array
				// craete an empty array to store the longest
				List<Integer> longestLength = new ArrayList<>();
				for (List<Integer> arr : arrays) {
				    if (arr.size() > longestLength.size()) {
				        // check if the current array hase the longest size than the last one
				        // if yes, update the last one
				        longestLength = arr;
				    }
				}
				// at the end print the result.
				System.out.println("longestLength = " + longestLength);
				
				//return 0;
				
				/*int[] num= {5,6,7,-1,-5,-2,8,9,-10,11};
				
				if (num.length == 0) {
					return 0;
				}
			 
				Set<Integer> set = new HashSet<Integer>();
				int max = 1;
			 
				for (int e : num)
					set.add(e);
			 
				for (int e : num) {
					System.out.println("e is "+e);
					int left = e - 1;
					int right = e + 1;
					int count = 1;
			 
					while (set.contains(left)) {
						count++;
						set.remove(left);
						left--;
					}
			 
					while (set.contains(right)) {
						count++;
						set.remove(right);
						right++;
					}
			 
					max = Math.max(count, max);
				}
			 //System.out.println(num);
				return max;
				
				
				/*int []arr={10,9,8,-7,6,5,4,2,3,-2,1};
				int maxSequenceStartIndex = 0;
		        int maxSequenceLength = 0;
		        int currentSequenceStartIndex = 0;
		        int currentSequenceLength = 0;
		        for (int i = 0; i < arr.length; i++) {
		            if(arr[i] < 0) {
		                if(currentSequenceLength > maxSequenceLength) {
		                    maxSequenceLength = currentSequenceLength;
		                    maxSequenceStartIndex = currentSequenceStartIndex;
		                }
		                currentSequenceStartIndex = i + 1;
		                currentSequenceLength = 0;
		            }
		            else {
		                currentSequenceLength++;
		            }
		        }
		        
		        System.out.println(currentSequenceLength+"===="+maxSequenceLength+"---"+maxSequenceStartIndex+";;;"+currentSequenceStartIndex);
		        if(currentSequenceLength > maxSequenceLength) {
		            maxSequenceStartIndex = currentSequenceStartIndex;
		            maxSequenceLength = currentSequenceLength;
		        }
				for(int i=maxSequenceStartIndex;i<maxSequenceStartIndex+maxSequenceLength;i++) {
					System.out.print(arr[i] + " ");
				}
				System.out.println();
				System.out.println("Length is "+maxSequenceLength);*/
			}



		}




