package testCasesUsingPom;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class EditLeadTC_TC002 extends ProjectMethods{

	@BeforeTest()
	public void setData() {
		testCaseName="TC002_EditLead";
		testCaseDesc="Edit lead test case";
		category="smoke";
		author="Bala";
		Data="editLead";
	}

	@Test (dataProvider="testData")
	public void createLead(String loginName,String password, String Companyname, String verifyCompanyName1) throws InterruptedException
	{
		new LoginPage().typeUserName(loginName).typePassword(password).clickLogin().clickLink().clickLeadsTab().clickFindLeads().clickFindLeadsButton()
		.clickFirstResult().clickEditButton().changeCompanyName(Companyname).clickUpdate().verifyCompanyname(verifyCompanyName1);

	}

}
