package week2.day2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexCreditCardNum {

	public static void main(String[] args) {
		passWord();
		//adhaarNumberCheck();
		//cardNumberCheck();


	}
	
	public static void cardNumberCheck() {
		System.out.println("Enter card number to validate");
		Scanner scan=new Scanner(System.in);
		String s=scan.next();
		//String se="\\d{4} \\d{4} \\d{4} \\d{4}";
		String reg="4\\d{12} (\\d{3})?";
		Pattern p=Pattern.compile("\\d{13,16}");
		Matcher m=p.matcher(s);
		boolean x=m.matches();
		if(x==true)
		{
			System.out.println("The credit card number is valid "+s);
		}
		else
			System.out.println("Card number is not valid " +x);
	}
	
	public static void adhaarNumberCheck() {
		System.out.println("Enter adhaar number to validate");
		Scanner scan=new Scanner(System.in);
		String s=scan.next();
		Pattern p=Pattern.compile("\\d{12}");
		Matcher m=p.matcher(s);
		boolean x=m.matches();
		if(x==true)
		{
			System.out.println("Adhaar number is valid");
		}
		else
			System.out.println("Card number is not valid");
	}
	
	public static void validateMailId() {
		System.out.println("Enter your mail id");
		Scanner scan=new Scanner(System.in);
		String s=scan.next();
		Pattern p=Pattern.compile("(.*[A-Za-z0-9])@hcl.com");
		Matcher m=p.matcher(s);
		boolean x=m.matches();
		if(x==true)
		{
			System.out.println("Mail id is valid "+x);
		}
		else
			System.out.println("Not a valid mail id ");
	}

	public static void passWord() {
		System.out.println("Enter your password");
		Scanner scan=new Scanner(System.in);
		String s=scan.next();
		Pattern p=Pattern.compile("(.*[A-Za-z0-9])(([a-z]){2,}).{8,}");
		Matcher m=p.matcher(s);
		boolean x=m.matches();
		if(x==true)
		{
			System.out.println("password is strong "+x);
		}
		else
			System.out.println("Not a valid password ");
	}
}
