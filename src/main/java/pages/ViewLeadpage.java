package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadpage extends ProjectMethods{

	public ViewLeadpage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="viewLead_companyName_sp")
	WebElement eleCompanyName;
	public ViewLeadpage verifyCompanyname(String data) {

		//getText(eleCompanyName).replaceAll("[^a-z]", "");
		verifyExactText(eleCompanyName, data);
		return this;
	}

	@FindBy(how=How.XPATH,using="//a[text()='Edit']")
	WebElement eleClickedit;
	public EditLeadPage clickEditButton() {
		click(eleClickedit);
		return new EditLeadPage();

	}
	
	@FindBy(how=How.LINK_TEXT,using="Duplicate Lead")
	WebElement eleClickDuplicate;
	public DuplicateLeadPage ClickDuplicateButton() {
		click(eleClickDuplicate);
		return new DuplicateLeadPage();

	}
	
	@FindBy(how=How.LINK_TEXT,using="Delete")
	WebElement eleDeleteBtn;
	public MyLeadsPage deleteLeadButton() {
		click(eleDeleteBtn);
		return new MyLeadsPage();
	}
}
