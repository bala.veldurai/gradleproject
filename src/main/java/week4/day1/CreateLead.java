package week4.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.findElement(By.id("id"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("XYZ");
		driver.findElementById("createLeadForm_firstName").sendKeys("Test");
		driver.findElementById("createLeadForm_lastName").sendKeys("LastTest");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Test1");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Test2");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("abc");
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select source1=new Select(source);
		source1.selectByVisibleText("Employee");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("AllTest");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
		WebElement country=driver.findElementById("createLeadForm_industryEnumId");
		Select country1=new Select(country);
		country1.selectByVisibleText("Health Care");
		WebElement ownerShip = driver.findElementById("createLeadForm_ownershipEnumId");
		Select ownerShip1=new Select(ownerShip);
		ownerShip1.selectByVisibleText("Corporation");
		driver.findElementById("createLeadForm_sicCode").sendKeys("1020325");
		driver.findElementById("createLeadForm_description").sendKeys("Testing reference");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Important notes");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("0000");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("004");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("004");
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");
		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
		Select currency1=new Select(currency);
		currency1.selectByVisibleText("BRR - Brazil");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("1000");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("dollar");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("ContactPerson");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://google.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("SampleTest");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("OMR, Chennai");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Kanchipuram");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		WebElement province = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select province1=new Select(province);
		province1.selectByVisibleText("Arkansas");
		WebElement country2 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select country21=new Select (country2);
		country21.selectByVisibleText("Algeria");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("603103");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("603103");
		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select market1=new Select(market);
		market1.selectByVisibleText("Car and Driver");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9999999999");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("test@gmail.com");
		driver.findElementByName("submitButton").click();
		List<WebElement> rowsTable = driver.findElementsByTagName("tr");
		for (int i = 0; i < rowsTable.size()-1; i++) {
			
			if(rowsTable.get(i).getText().contains("First name"))
					{
				System.out.println("The first name is "+rowsTable.get(i).getText());
					}
		}
	}

}
