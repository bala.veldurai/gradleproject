package week6;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SwapTwoStrings {

	public static void main(String[] args) {

		Scanner input=new Scanner(System.in);
		System.out.println("Enter first string");
		String fString = input.nextLine();
		System.out.println("Enter second string");
		String secString = input.nextLine();
		
		fString=fString+secString;
		secString=fString.substring(0,fString.length()-secString.length());
		System.out.println("sec string is "+secString);
		fString=fString.substring(secString.length());
		System.out.println("First string is "+fString);
				
	}

}
