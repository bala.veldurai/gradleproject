package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertsFramesCW {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		WebDriver frame = driver.switchTo().frame("iframeResult");
		frame.findElement(By.xpath("//button[text()='Try it']")).click();
		Alert alert = frame.switchTo().alert();
		alert.sendKeys("Bala");
		alert.accept();
		System.out.println(frame.findElement(By.id("demo")).getText());

	}

}