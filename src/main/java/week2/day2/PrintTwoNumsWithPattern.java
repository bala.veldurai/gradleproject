package week2.day2;

import java.util.Scanner;

public class PrintTwoNumsWithPattern {

	public static void main(String[] args) {
		int i;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter two numbers ");
		int n1=sc.nextInt();
		int n2=sc.nextInt();
		for(i=n1; i<=n2;i++)
		{
			if(i%3==0 && i%5==0)
				System.out.print("FIZZBUZZ"+" ");

			else if(i%5==0)
				System.out.print("BUZZ"+" ");
			else if(i%3==0)
				System.out.print("FIZZ"+" ");
			else
				System.out.print(i+" ");			

		}
	}
}
