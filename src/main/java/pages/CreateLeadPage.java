package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="createLeadForm_companyName")
	WebElement eleCompanyName;
	@And("Enter company name as (.*)")
	public CreateLeadPage typeCompanyname(String data) {
		type(eleCompanyName, data);
		return this;
	}
		
	@FindBy(id="createLeadForm_lastName")
	WebElement eleLastName;
	@And("Enter Last name as (.*)")
	public CreateLeadPage typeLastName(String data) {
		type(eleLastName, data); 
		return this;
	}
	
	@FindBy(id="createLeadForm_firstName")
	WebElement eleFirstName;
	@And("Enter First name as (.*)")
	public CreateLeadPage typeFirstName(String data) {
		type(eleFirstName, data); 
		return this;
	}
	
	@FindBy(how=How.NAME, using="submitButton")
	WebElement eleSubmit;
	@When("click on createLead button")
	public ViewLeadpage clickSubmit() {
		click(eleSubmit); 
		return new ViewLeadpage();
	}
	
}
