package week2.day2;

public class PrimeNumber {

	public static void main(String[] args) {

		//primeNumberBetweenTwo(2,20);
		//primeNumber(30);
		//anotherPrimeMethod(20);
		anotherPrimeMethod(1,20);
	}
/*	public static void primeNumberBetweenTwo(int a, int b) {

		int flag = 0;

		for(int i=a; i<=b;i++)
		{
			for(int j=2; j<=a; j++)
			{
				if(i%j==0) {
					flag=0;
					break;
				}
				else 
				{
					flag=1;
				}
			}

			if(flag==1)
				System.out.println(i);

		}
	}

	public static void primeNumber(int x)
	{
		int y,z, flag=0;
		int sum=2;
		System.out.println(sum);

		for(y=3; y<=x; y++)
		{
			for(z=2; z<y; z++)
			{
				if(y%z==0)
				{
					flag=0;
					break;
				}

				else {
					flag=1;	

				}
			}
			if(flag==1)
			{

				System.out.println(y);
			}
		}
	}*/
	
	public static void anotherPrimeMethod(int a) {
		int i,j;
		
		for(i=1; i<=a;i++)
		{
			int count=0;
			for(j=i;j>=1;j--)
			{
				if(i%j==0)
				{
					count=count+1;
				}
			}
			
			if(count==2)
			{
				System.out.println(i);
			}
		}
		
	}
	
	public static void anotherPrimeMethod(int a,int b) {
		int i,j;
		
		for(i=a; i<=b;i++)
		{
			int count=0;
			for(j=i;j>=1;j--)
			{
				if(i%j==0)
				{
					count=count+1;
				}
			}
			
			if(count==2)
			{
				System.out.println(i);
			}
		}
		
	}

}
