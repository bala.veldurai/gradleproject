package testCasesUsingPom;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class DuplicateLeadTC_TC003 extends ProjectMethods {
	
	@BeforeTest()
	public void setData() {
		testCaseName="TC003_DuplicateLead";
		testCaseDesc="Duplicate lead test case";
		category="smoke";
		author="Bala";
		Data="LoginData";
	}
	
	@Test (dataProvider="testData")
	public void duplicateLead(String loginName, String password ) throws InterruptedException {
		new LoginPage().typeUserName(loginName).typePassword(password).clickLogin().clickLink().clickLeadsTab().clickFindLeads().clickFindLeadsButton()
		.clickFirstResult().ClickDuplicateButton().clickCreateLead();
		
	}

}
