package week4.day1;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows");
		int value = sc.nextInt();
		int s=1;
		//char s='a';
		for(int i=1;i<=value;i++) {
			for(int j=1;j<=i;j++)
			{
				System.out.print(s++ + " ");
			}
			System.out.println();
		}
	}
}
