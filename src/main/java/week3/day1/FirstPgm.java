package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class FirstPgm {

	private static final Exception ElementNOtFound = null;

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		try {
			driver.get("http://leaftaps.com/opentaps/control/main");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			try {
				driver.findElementById("username").sendKeys("DemoSalesManager");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				System.out.println("Element not found exception");
				//throw new RuntimeException();
				throw new ElementNotSelectableException("Not displayed");
			}
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("HCL");
			driver.findElementById("createLeadForm_firstName").sendKeys("Test1");
			driver.findElementById("createLeadForm_lastName").sendKeys("test2");
			WebElement ele=driver.findElementById("createLeadForm_dataSourceId");
			//to handle dropdown
			Select dropDown=new Select(ele);
			dropDown.selectByVisibleText("Direct Mail");
			WebElement ele1=driver.findElementById("createLeadForm_marketingCampaignId");
			Select dropDown1=new Select(ele1);
			//to store dropdown elements
			List<WebElement> allOptions=dropDown1.getOptions();
			int size1=allOptions.size();
			dropDown1.selectByIndex(size1-2);
			driver.findElementByName("submitButton").click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			driver.close();

		}


	}

}
