package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringSortUsingList {


	/*sort string using List
	change to upper case and print in reverse order
	remove duplicate and print in sorted order*/

	public static void main(String[] args) {
		String s="AmazonIndia";
		char[] eachChar=s.toCharArray();
		//int size=eachChar.length;
		List<Character> charList=new ArrayList<>();
		for (Character character : eachChar) {
			charList.add(character);

		}
		Collections.sort(charList);
		//System.out.println("Sorted Array is "+charList);
		System.out.println("The sorted array is ");
		for (Character eChar : charList) {
			System.out.println(eChar);

		}

		//print in lower case
		String lowCase= s.toLowerCase();
		char[] eachChar1=lowCase.toCharArray();

		List<Character> charList1=new ArrayList<>();

		for (char x : eachChar1) {
			charList1.add(x);

		}
		Collections.reverse(charList1);
		System.out.println("Lower case in reverse order is "+charList1);

	


	//remove duplicate in array and sort in chronological order

	
		
	for(int i=0;i<charList1.size();i++)
	{
		for(int j=i+1;j<charList1.size();j++)
		{
			if(charList1.get(i).equals(charList1.get(j)))
			{
				charList1.remove(j);
				
			}
		}

	}
	System.out.println("After removed duplicates "+charList1);


	}

}
