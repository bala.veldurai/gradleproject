package week5;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class CreateHtmlReport {




	public static void main(String[] args) throws IOException {
		

		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/results.html");
		html.setAppendExisting(false);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);


		ExtentTest test = extent.createTest("TC001_LoginAndLogout", "Verify Login and Logout function");
		test.assignCategory("Smoke");
		test.assignAuthor("Bala");


		//testcase step level
		test.pass("Browser launched successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps./img1.png").build());
		test.pass("The data DemoSalesManager entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps./img2.png").build());
		test.pass("The data crmsfa entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps./img3.png").build());
		test.pass("Button clicked successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps./img4.png").build());
		test.pass("Logged out successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps./img5.png").build());

		extent.flush();

	}

}
