package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);

	}

	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']")
	WebElement eleFindButton;
	public FindLeadsPage clickFindLeadsButton() {
		click(eleFindButton);
		return this;

	}

	@FindBy(how=How.XPATH, using="(//a[@class='linktext'])[4]")
	WebElement eleFirstLink;
	public ViewLeadpage clickFirstResult() throws InterruptedException {
		Thread.sleep(1000);
		click(eleFirstLink);
		return new ViewLeadpage();
	}
	
	


}
