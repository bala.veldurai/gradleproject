package week5;

public class FourCrossFourMetrix {

	public static void main(String[] args) {
		int b=1;

		for (int i=1; i<=4; i++) {

			for(int j=1;j<=4;j++) {

				System.out.print( b++ + " ");
			}
			System.out.println(" ");
		}				


		int a[][]= { {1, 2, 3, 4, 5},
					{5, 6 ,7 ,8 , 9},
					{9, 10, 11, 12, 13},
					{13, 14, 15, 16, 17},
					{18, 19, 20, 21, 22} };

		
		System.out.println("Spiral matrix form is ");
		int rowsM=5; //ending row index
		int colN=5; //ending col index

		int  sRowsK = 0, sColL=0;//starting row and column
		int i=0;
		while(sRowsK<rowsM && sColL<colN) {
			//first row in matrix
			for( i=sColL;i<colN;i++) {
				System.out.print(a[sRowsK][i]+ " ");
			}
			sRowsK++;

			//print last column
			for(i=sRowsK;i<rowsM;i++) {
				System.out.print(a[i][colN-1]+ " ");
			}
			colN--;

			for(i=colN-1;i>=sColL;i--) {
				System.out.print(a[rowsM-1][i]+ " ");
			}
			rowsM--;

			for(i=rowsM-1;i>=sRowsK;i--) {
				System.out.print(a[i][sColL]+ " ");
			}
			sColL++;
		}

	}
}


