package week3.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IRCTCDropDownSelection {

	public static void main(String[] args) {
		List<String> countrys=new ArrayList<>();
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		WebDriverWait wait=new WebDriverWait(driver, 0);
		
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement ele=driver.findElementById("userRegistrationForm:countries");
		Select country=new Select(ele);

		List<WebElement> sc=country.getOptions();
		for (int i=0;i<sc.size();i++ ) {
			String s=sc.get(i).getText();
			if(s.startsWith("E"))
			{
				countrys.add(s);
			}
		}
		String Secondvalue=countrys.get(1);
		country.selectByVisibleText(Secondvalue);
	}

}
