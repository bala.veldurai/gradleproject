package testCasesUsingPom;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class LoginAndLogoutTC_TC005 extends ProjectMethods {
	
	@BeforeTest()
	public void setData() {
		testCaseName="TC005_LoginAndLogout";
		testCaseDesc="Verify Login and Logout";
		category="smoke";
		author="Bala";
		Data="LoginData";
	}
	
	@Test (invocationCount=1, dataProvider="testData")
	public void createLead(String loginName,String password)
	{
		new LoginPage().typeUserName(loginName).typePassword(password).clickLogin().clickLogout();
		
	}

}
