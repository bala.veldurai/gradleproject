package week1.day2;

import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		strPalindrome();
		strBuffer();
		

	}
	
	//palindrome wihtput stringbuffer
	public static void strPalindrome() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the string ");
		String str=scan.next();

		String reverse="";
		for(int i=(str.length()-1); i>=0;i--)
		{
			reverse=reverse+str.charAt(i);
			//System.out.println(reverse);
		}
		if(reverse.equals(str))
		{
			System.out.println("String is palindrome");
		}
		else {
			System.out.println("String is not palindrome");
		}
		
		
	}
	//palindrome with string buffer
	public static void strBuffer() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the string ");
		String str=scan.next();
	StringBuffer sb=new StringBuffer(str);
	StringBuffer ans=sb.reverse();
	if(ans.equals(sb))
	{
		System.out.println("String is palindrome");
	}
	else {
		System.out.println("String is not palindrome");
	}
		
	}

}
