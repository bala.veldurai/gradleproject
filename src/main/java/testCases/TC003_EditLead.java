package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC003_EditLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName="TC003_EditLead";
		testCaseDesc="Edit a Lead";
		category="smoke";
		author="Bala";
	}
		

	@Test(groups= {"smoke"}, dataProvider="editLead")
	public void editLead(String name, String company) throws InterruptedException
	{
		
		WebElement leads = locateElement("linktext", "Leads");
		click(leads);
		WebElement findLeads = locateElement("linktext", "Find Leads");
		click(findLeads);
		WebElement firstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(firstName, name);
		WebElement button1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(button1);
		Thread.sleep(500);
		WebElement linkClick = locateElement("xpath","(//a[@class='linktext'])[4]");
		click(linkClick);
		Thread.sleep(1000);
		System.out.println(getTitle());
		String expectedName=locateElement("viewLead_companyName_sp").getText();
		WebElement editLink = locateElement("xpath","//a[text()='Edit']");
		click(editLink);
		WebElement editName = locateElement("updateLeadForm_companyName");
		//String expectedName=editName.getText();
		locateElement("updateLeadForm_companyName").clear();
		type(editName, company);		
		locateElement("xpath","(//input[@type='submit'])[1]").click();
		WebElement getText1 = locateElement("viewLead_companyName_sp");
		verifyExactText(getText1, expectedName);
		/*System.out.println(getText1 + "value is " );
		if(getText(getText1).equals("TCS"))
		{
			System.out.println("Company name changed successfully");
		}*/
		//driver.quit();
	}
	
	
	@DataProvider(name="editLead")
	public Object[][] passData() {
		Object[][] datas=new Object[2][2];
		datas[0][0]="Bala";
		datas[0][1]="CTS";

		datas[1][0]="Sundari";
		datas[1][1]="Wipro";
				
		return datas;
	}
	
}
