package week3.day2;

public class ArraySort {

	public static void main(String[] args) {

		int temp;
		int[] intArray= {20,340,21,879,92,21,474,83647,-200};
		for(int i=0;i<intArray.length;i++)
		{
			for(int j=0;j<intArray.length;j++)
			{
				if(intArray[i]<intArray[j])
				{
					temp=intArray[i];
					intArray[i]=intArray[j];
					intArray[j]=temp;
				}
			}
		}
		System.out.println("Second largest number is "+intArray[intArray.length-2]);
	}

}
