package week3.day2;

public class ArmstrongNumber {

	public static void main(String[] args) {
		int i,temp, rem;
		System.out.println("Armstrong numbers are: ");
		for(i=100; i<=1000; i++)
		{
			temp=i;
			int sum=0;
			while(temp>0)
			{
				rem=temp%10;
				sum=sum+(rem*rem*rem);
				temp=temp/10;
			}
			if(sum==i)
			{
				System.out.println(i);
			}
		}

	}

}
