package week6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoveSpace {

	public static void main(String[] args) {

		String input=" aa bbbbb   ccc  d  ";
		System.out.println(input.trim().replaceAll("\\s{2,}", " "));

		String input1="Bala";
		Pattern p=Pattern.compile(".*[a-zA-Z]{4,}");
		Matcher m=p.matcher(input1);
		boolean matches = m.matches();
		System.out.println(matches);


	}

}
