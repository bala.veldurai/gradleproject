package projectDay;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class FaceBookLoginSample extends SeMethods{

	@Test	
	public void faceBook() {
		startApp("chrome", "https://www.facebook.com/");
		WebElement email = locateElement("email");
		type(email, "ramnatrajan@gmail.com");
		WebElement pass = locateElement("pass");
		type(pass, "Ayogya@1984");
		WebElement logIn = locateElement("u_0_2");
		click(logIn);
		//keys tab 
		WebElement searchBox = locateElement("class", "_1frb");
		type(searchBox, "testleaf");
		WebElement searchButton = locateElement("xpath", "//button[@data-testid='facebar_search_button']");
		click(searchButton);
		WebElement testLeafLink = locateElement("xpath", "//div[text()='TestLeaf']");
		//String leafText = getText(testLeafLink);
		verifyExactText(testLeafLink, "TestLeaf");
		click(testLeafLink);
		WebElement likeButtonText = locateElement("xpath", "(//button[@type='submit'])[2]");
		String likeText=getText(likeButtonText);
		if(likeText.equals("Like"))
		{
			click(likeButtonText);
		}
		else if(likeText.equals("Liked"))
		{
			System.out.println("TestLeaf is already liked");

		}
		click(testLeafLink);
		verifyTitle("TestLeaf");	
		WebElement likes = locateElement("xpath", "(//div[@class='_4bl9'])[3]");
		System.out.println("Number of likes are "+getText(likes));
		closeBrowser();
	}

}
