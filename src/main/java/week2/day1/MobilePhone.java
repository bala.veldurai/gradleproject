package week2.day1;

public class MobilePhone extends TeleCom implements TeleDesign {

	public void sendSMS() 
	{ 
		System.out.println("Send SMS");
	}

	public void musicPlayer() 
	{
		System.out.println("Play music");
	}

	@Override
	public void acceptTC() {
		System.out.println("Accepted Terms and Conditions");		
	}

	@Override
	public void support4G() {
		System.out.println("4G supported");		

	}



}
