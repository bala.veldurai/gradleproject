package testCasesTestngExecution;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC005_DeleteLead extends ProjectMethods{
	
	@BeforeTest(groups= {"sanity"})//, dependsOnGroups= {"smoke"})
	public void setData() {
		testCaseName="TC005_DeleteLead";
		testCaseDesc="Delete a Lead";
		category="smoke";
		author="Bala";
	}

	@Test(groups= {"sanity"})//, dependsOnGroups= {"smoke"})
	public void deletedLeadPgm () {
		
		WebElement leads = locateElement("linktext", "Leads");
		click(leads);
		WebElement findLeads = locateElement("linktext", "Find Leads");
		click(findLeads);
		locateElement("xpath", "//span[text()='Phone']").click();
		WebElement locateElement = locateElement("name", "phoneNumber");
		type(locateElement, "100");
		WebElement buttonClick = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonClick);
		WebElement firstresultClick = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String text = getText(firstresultClick);
		System.out.println("First resulting lead id is "+text);
		//webdriverwaitFunction 
		/*WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(firstresultClick));
		click(firstresultClick);*/
		
		locateElement("xpath", "(//a[@class='linktext'])[4]").click();
		locateElement("xpath", "//a[text()='Delete']").click();
		WebElement findLeads1 = locateElement("linktext", "Find Leads");
		click(findLeads1);
		WebElement locateElement2 = locateElement("xpath", "//label[text()='Lead ID:']/following::input[@class=' x-form-text x-form-field']");
		type(locateElement2, text);
		locateElement("xpath", "//a[text()='Find Leads']").click();
		/*WebElement locateElement2 = locateElement("xpath", "//label[text()='Lead ID:']/following::input[@class=' x-form-text x-form-field']");
		type(locateElement2, text);
		locateElement("xpath", "//a[text()='Find Leads']").click();*/

		//driver.close();

	}

}
