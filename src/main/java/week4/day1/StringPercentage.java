package week4.day1;

import java.util.Scanner;

public class StringPercentage {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string ");
		String text=sc.nextLine();

		int space=0, Uletter=0, sLetter=0, number=0;
		int size=text.length();
		//System.out.println(text);
		for (int i = 0; i < size; i++) 
		{
			char c=text.charAt(i);
			//System.out.println("the chars are "+c);

			if(Character.isUpperCase(c))
			{
				Uletter++;
			}
			else if(Character.isLowerCase(c))
			{
				sLetter++;
			}
			else if(Character.isDigit(c))
			{
				number++;
			}
			else if(Character.isWhitespace(c))
			{

				space++;
			}
			else 
			{
				space++;
			}

		}

		System.out.println("Number of uppercase letters is "+Uletter+ ". So percentage is "+ (double)(Math.round((Uletter*100/size)*100.0)/100)+ "%");
		System.out.println("Number of uppercase letters is "+Uletter+ ". So percentage is "+ ((float)Uletter*100/(float)size)+ "%");
		System.out.println("Number of lowercase letters is "+sLetter+ ". So percentage is "+ ((float)sLetter*100/(float)size)+ "%");
		System.out.println("Number of digit letters is "+number+ ". So percentage is "+ ((float)number*100/(float)size)+ "%");
		System.out.println("Number of other character is "+space+ ". So percentage is "+ ((float)space*100/(float)size)+ "%");
	}

}