package week0;

public class DiffDataType {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//integer 
		int i=647895855;
		System.out.println("Interger default value is "+ i);
		
		//long
		long l=188888888;
		System.out.println("Long value is "+ l);
		
		// double
		double d=955555548;
		System.out.println("double value is "+ d);
		
		//float
		float f=456.23f;
		System.out.println("float value is "+ f);
		
		//char
		char c='g';
		System.out.println("char value is "+ c);
		
		//boolean
		boolean b=true;
		if (b==true)
		{
			System.out.println("Value is true");
		}
		else
			System.out.println("value is false");
		
		//byte
		byte bv=126;
		System.out.println("byte value is " + bv);
		
		//short
		short sh=12655;
		System.out.println("short value is " + sh);
		
	}

}
