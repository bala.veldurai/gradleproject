package projectDay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import wdMethods.SeMethods1;

public class FaceBookLogin extends SeMethods1{

	@Test	
	public void faceBook() throws InterruptedException {
		startApp("chrome", "https://www.facebook.com/");
		WebElement email = locateElement("email");
		type(email, "ramnatrajan@gmail.com");
		WebElement pass = locateElement("pass");
		type(pass, "Ayogya@1984");
		WebElement logIn = locateElement("xpath","//input[@type='submit']");
		click(logIn);
		WebElement searchBox = locateElement("class", "_1frb");
		type(searchBox, "testleaf");
		WebElement searchButton = locateElement("xpath", "//button[@data-testid='facebar_search_button']");
		click(searchButton);
		WebElement testLeafLink = locateElement("xpath", "//div[text()='TestLeaf']");
		//String leafText = getText(testLeafLink);
		verifyExactText(testLeafLink, "TestLeaf");
		//click(testLeafLink);
		WebElement likeButtonText = locateElement("xpath", "(//button[@type='submit'])[2]");
		String likeText=getText(likeButtonText);
		if(likeText.equals("Like"))
		{
			click(likeButtonText);
		}
		else if(likeText.equals("Liked"))
		{
			System.out.println("TestLeaf is already liked");
		}
		click(testLeafLink);
		Thread.sleep(1000);
		verifyTitle("(2) TestLeaf - Home");	
		WebElement likes = locateElement("xpath", "(//div[@class='_4bl9'])[3]");
		System.out.println("Likes are "+getText(likes));
		closeBrowser();
	}

}
