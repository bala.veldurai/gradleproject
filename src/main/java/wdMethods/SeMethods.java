package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bsh.org.objectweb.asm.Type;
import cucumber.api.java.en.Given;
import utils.Reporter;


public class SeMethods extends Reporter implements WdMethods{
	public static RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver  = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver  = new FirefoxDriver();
			}else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer.exe");
				driver  = new InternetExplorerDriver();
			}else if(browser.equalsIgnoreCase("edge")) {
				System.setProperty("webdriver.edge.driver", "./drivers/MicrosoftWebDriver.exe");
				driver  = new EdgeDriver();	
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The Browser "+browser+" launched successfully", "pass");
		}
		catch (WebDriverException e) {
			//System.err.println("WebDriverException has occured");
			reportStep("The Browser "+browser+" not launched successfully", "fail");
			throw new RuntimeException("WebDriverException has occured");

		}finally {
			takeSnap();
		}
	}


	public WebElement locateElement(String locator, String locValue) {

		try {
			switch (locator) {
			case "id":
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementById(locValue);
			case "class": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementByClassName(locValue); }
			case "xpath": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementByXPath(locValue); }
			case "linktext": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementByLinkText(locValue);	}
			case "partiallinktext": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementByPartialLinkText(locValue);	}
			case "name": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementByName(locValue);	}
			}
		} catch (NoSuchElementException e) {

			System.err.println("NoSUchElementException has occured");
	reportStep("The element with locator "+locator+" and with value "+locValue+" not found.", "fail");

			throw new RuntimeException("NoSuchElementException has occured");
		}
		catch (Exception e) {

			System.err.println("Exception has throws");
			reportStep("The element with locator "+locator+" and with value "+locValue+" not found.", "fail");

			throw new RuntimeException("Exception has thrown");
		}
		return null;
	}
	
	public List<WebElement> locateElements(String locator, String locValue) {

		try {
			switch (locator) {
			case "id":
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementsById(locValue);
			case "class": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementsByClassName(locValue); }
			case "xpath": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementsByXPath(locValue); }
			case "linktext": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementsByLinkText(locValue);	}
			case "partiallinktext": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementsByPartialLinkText(locValue);	}
			case "name": {
				//reportStep("The element with locator "+locator+" and with value "+locValue+" has found.", "pass");
				return driver.findElementsByName(locValue);	}
			}
		} catch (NoSuchElementException e) {

			System.err.println("NoSUchElementException has occured");
	reportStep("The element with locator "+locator+" and with value "+locValue+" not found.", "fail");

			throw new RuntimeException("NoSUchElementException has occured");
		}
		catch (Exception e) {

			System.err.println("Exception has throws");
			reportStep("The element with locator "+locator+" and with value "+locValue+" not found.", "fail");

			throw new RuntimeException("Exception has thrown");
		}
		return null;
	}
	
	
		public String getTitle() {		
		String title=" ";
		try {
			title = driver.getTitle();
			reportStep("Page title is "+title, "pass");

		} catch (WebDriverException e) {
			reportStep("WebDriverException"+e.getMessage(), "fail");
		}
		return title;
	}


	public WebElement locateElement(String locValue) {		
		try {

			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			System.err.println("No such element exception");
			throw new RuntimeException("No such element exception");
		} catch (Exception e) {
			System.out.println("Exception has thrown ");
			throw new RuntimeException("Exception has thrown");

		}
	}

	/*public void webDriverWait(WebElement ele) {
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathExpression));

	}*/



	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data: "+data+" is entered Successfully", "pass");

		} catch (InvalidElementStateException e) {
			reportStep("The data: "+data+" could not entered", "fail");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException"+e.getMessage());
		}finally {
			takeSnap();
		}
	}	


	public void click(WebElement ele) {
		String text=" ";
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));	
			text=ele.getText();
			ele.click();
			reportStep("The Element "+text+" is clicked Successfully", "pass");
		} catch (InvalidElementStateException e) {
			reportStep("The element: "+text+" could not clicked","fail");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException"+e.getMessage());
		} 
	}


	public String getText(WebElement ele) {

		String eleText =" ";
		try {
			eleText = ele.getText();
			reportStep("Taken text successfully "+eleText , "pass");
		} catch (WebDriverException e) {
			reportStep("Get text is failed " , "fail");

			System.out.println("WebDriverException"+e.getMessage());
		}
		return eleText;
	}

	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dropDown=new Select(ele);
			dropDown.selectByVisibleText(value);
			reportStep("The dropdown is selected with text ", "pass");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException"+e.getMessage());
		}finally {
			takeSnap();
		}
	}

	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dropDown=new Select(ele);
			dropDown.selectByIndex(index);
			reportStep("The dropdown is selected with index ", "pass");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException"+e.getMessage());
		}finally {
			takeSnap();

		}
	}

	public boolean verifyTitle(String expectedTitle) {
		boolean result=false;
		String title=driver.getTitle();
		try {
			if(title.equals(expectedTitle)) {
				result=true;
				reportStep(" The expected title "+expectedTitle+" matches the actual "+getTitle(),"pass");
			}else {
				reportStep(" The expected title "+expectedTitle+" doesn't matches the actual "+getTitle(), "fail");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 
		return result;

	}

	public void verifyExactText(WebElement ele, String expectedText) {

		try {
			String actualText=ele.getText();
			if(expectedText.equals(actualText))
			{
				reportStep(expectedText+" Expected and actual texts are matches "+actualText, "pass");
			}
			else {
				reportStep( expectedText+" The expected text doesn't matches the actual "+actualText , "fail");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} finally {
			takeSnap();
		}

	}

	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			if(getText(ele).contains(expectedText)) {
				reportStep("The expected text contains the actual "+expectedText, "pass");
			}else {
				reportStep("The expected text doesn't contain the actual "+expectedText, "fail");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 
	}

	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			String actualAttribute = ele.getAttribute(attribute);
			if(actualAttribute.equals(value))
			{
				reportStep("The expected attribute :"+attribute+" value matches the actual "+value, "pass");
			}else {
				reportStep("The expected attribute :"+attribute+" value does not matches the actual "+value, "fail");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 
	}

	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String actualAttribute = ele.getAttribute(value);
			if(actualAttribute.contains(attribute))
			{
				reportStep("The expected attribute :"+attribute+" value contains the actual "+value, "pass");
			}else {
				reportStep("The expected attribute :"+attribute+" value does not contains the actual "+value, "fail");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		}
	}

	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				reportStep("The element "+ele+" is selected", "pass");
			} else {
				reportStep("The element "+ele+" is not selected", "fail");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		}

	}

	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				reportStep("The element "+ele+" is visible", "pass");
			} else {
				reportStep("The element "+ele+" is not visible","fail");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 
	}

	public void switchToWindow(int index) {
		try {
			Set<String> allWindowHandles = driver.getWindowHandles();
			List<String> allHandles = new ArrayList<String>();
			allHandles.addAll(allWindowHandles);
			driver.switchTo().window(allHandles.get(index));
			reportStep("The driver moved to the given window by index "+index, "pass");
		} catch (NoSuchWindowException e) {
			reportStep("The driver could not move to the given window by index "+index, "fail");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		}
	}

	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("switch In to the Frame "+ele, "pass");
		} catch (NoSuchFrameException e) {
			reportStep("No such Frame "+ele, "fail");
			System.out.println("WebDriverException : "+e.getMessage());
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 
	}

	public void acceptAlert() {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.accept();
			reportStep("The alert "+text+" is accepted.", "pass");
		} catch (NoAlertPresentException e) {
			reportStep("There is no alert present.", "fail");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		}  


	}

	public void dismissAlert() {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.dismiss();
			reportStep("The alert "+text+" is dismissed.", "pass");
		} catch (NoAlertPresentException e) {
			reportStep("There is no alert present.","fail");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 

	}

	public String getAlertText() {
		
		
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			reportStep("Alert text is "+text, "pass");
		} catch (NoAlertPresentException e) {
			reportStep("There is no alert present.", "fail");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 
		return text;
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");		
			FileUtils.copyFile(src, desc);
			
		} catch (IOException e) {
			System.out.println("The snapshot could not be taken");
		}
		i++;
	}

	public void closeBrowser() {
		try {
			driver.close();
			reportStep("The browser is closed", "pass");
		} catch (Exception e) {
			reportStep("The browser could not be closed", "fail");
		}
	}

	public void closeAllBrowsers() {
		try {
			driver.quit();
			reportStep("The opened browsers are closed","pass");
		} catch (Exception e) {
			reportStep("The browsers could not be closed","fail");
		}

	}

}
