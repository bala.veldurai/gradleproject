package week5;

import java.util.Scanner;

public class DiagonalSum {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("The matrix size");
		int size=sc.nextInt();
		int diag=((size*size)-size)/2;
		int a[][]=new int[size][size];
		System.out.println("Array values are ");
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				a[i][j]=sc.nextInt();
			}
		}

		int[] upperArr=new int[diag];
		int[] lowerArr=new int[diag];
		int uppersum=0, lowersum=0, i=0, j=0, k=0, l=0, pos1=0;
		int pos2=size-1;
		for(i=0;i<size;i++) {
			for(j=0;j<size;j++) {
				if(a[i][j]!=a[pos1][pos2]) {

					if(j<pos2) {
						upperArr[k]=a[i][j];
						uppersum=uppersum+a[i][j];
						k++;
					}

					if(j>pos2) {
						lowerArr[l]=a[i][j];
						lowersum=lowersum+a[i][j];
						l++;
					}

				}
			}
			pos1=pos1+1;
			pos2=pos2-1;
		}
		System.out.println("upper sum is "+uppersum);
		System.out.println("lower sum is "+lowersum);



		for(i=0;i<diag;i++) {
			if(uppersum>lowersum) {
				System.out.print(upperArr[i]+ " , ");
			}
			else
			{
				System.out.print(lowerArr[i]);

			}
		}
	}
}


