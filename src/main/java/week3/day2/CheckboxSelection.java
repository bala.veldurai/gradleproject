package week3.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CheckboxSelection {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/checkbox.html");
		driver.manage().window().maximize();
		boolean value=driver.findElementByXPath("//label[contains(text(),'Selenium')]/following-sibling::input").isSelected();
		if(value==true)
		{
			System.out.println("Checkbox is selected");
			//driver.findElementByXPath("//label[contains(text(),'Selenium')]/following-sibling::input").click();
		}
		List<WebElement> eles=driver.findElementsByXPath("//label[contains(text(), 'DeSelect')]/following-sibling::input");
		for (WebElement webElement : eles) {
			System.out.println(webElement.getText());

			if(webElement.getText().equals("I am Selected")) {
		System.out.println(webElement.getText());
			}
		}

	}

}
