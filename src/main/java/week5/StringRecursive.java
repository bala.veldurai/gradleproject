package week5;

import java.util.Scanner;

public class StringRecursive {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string");
		String s=sc.nextLine();
		if(palString (s) )
			System.out.println("String is palindrome");
		else
			System.out.println("String is not palindrome");

	}

	public static boolean palString (String s) {


		if(s.length()==0|| s.length()==1)

			return true;


		if(s.charAt(0)==s.charAt(s.length()-1))
		{
			return palString (s.substring(1, s.length()-1));
		}
		return false;
	}

}
