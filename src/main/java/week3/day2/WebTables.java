package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTables {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/table.html");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//List<WebElement> listTable=driver.findElementsByXPath("section[@class='innerblock']//td");
		List<WebElement> tableRows=driver.findElementsByTagName("tr");
		System.out.println("Number of rows are "+tableRows.size());

		for (int i = 1; i < tableRows.size()-1; i++) {
			List<WebElement> rowsTable = tableRows.get(i).findElements(By.tagName("td"));
			String text=rowsTable.get(2).getText();
			System.out.println(text);
			if(text.contains("80")) {
				tableRows.get(i).findElements(By.tagName("td")).get(3).click();
			}
		}

		/*
		System.out.println("Number of rows are "+tableRows.size());
		List<WebElement> tableCol=driver.findElementsByTagName("td");
		System.out.println("Number of rows are "+tableCol.size());
		for (WebElement webElement : tableCol) 
		{
			if(webElement.getText().equals("80%"))
			{
				System.out.println("click checkbox");
			}

		}*/
	}

}


