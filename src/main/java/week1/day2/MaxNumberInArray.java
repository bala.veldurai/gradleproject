package week1.day2;

import java.util.Scanner;

public class MaxNumberInArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan=new Scanner(System.in);
		System.out.println("Enter array size ");
		int size=scan.nextInt();
		int[] arrayList=new int[size];
		System.out.println("Enter array numbers ");


		for(int i=0;i<size;i++)
		{
			arrayList[i]=scan.nextInt();		
		}

		for(int i=0;i<size;i++)
		{
			for(int j=1; j<size;j++)
			{
				if(arrayList[i]<arrayList[j])
				{
					int temp=arrayList[i];
					arrayList[i]=arrayList[j];
					arrayList[j]=temp;
				}
			}
		}

			System.out.println("Maximum number in the array is "+arrayList[0]);
		}
	}

