package projectDay;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;


public class BugFix {
	
		@Test
		public void mynthra() throws InterruptedException {

			// launch the browser
			System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.get("https://www.myntra.com/");

			// Mouse Over on Men
			WebElement elementMen = driver.findElementByLinkText("Men");
			Actions builder = new Actions(driver);
			builder.moveToElement(elementMen).perform();

			// Click on Jackets
			driver.findElementByXPath("//a[@href='/men-jackets']").click();


			// Find the count of Jackets
			
			String leftCount = driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span").getText().replaceAll("\\D", "");
			System.out.println("leftCount is "+leftCount);


			// Click on Jackets and confirm the count is same
			driver.findElementByXPath("//label[text()='Jackets']").click();
			

			// Wait for some time
			Thread.sleep(5000);

			// Check the count
			String rightCount = 
					driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span")
					.getText()
					.replaceAll("\\D", "");
			System.out.println("rightcount is "+rightCount);

			// If both count matches, say success
			if(leftCount.equals(rightCount)) {
				System.out.println("The count matches on either case");
			}else {
				System.err.println("The count does not match");
			}

			// Click on Offers
			//driver.findElementByXPath("//h3[text()='Offers']").click();

			// Find the costliest Jacket
			List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
			List<String> onlyPrice = new ArrayList<String>();

			for (WebElement productPrice : productsPrice) {
				onlyPrice.add(productPrice.getText().replaceAll("\\D", ""));
			}

			// Sort them 
			//Collections.sort(onlyPrice);
			String max = Collections.max(onlyPrice);

			// Find the top one
			System.out.println("Max price is "+max);
			
			//driver.close();
			
			//Choose allen solly
			driver.findElementByClassName("brand-more").click();
			Actions builder1=new Actions(driver);

			//driver.findElementByClassName("FilterDirectory-searchInput").sendKeys(" ",Keys.TAB);
			/*WebElement AllenSolly = driver.findElementByXPath("(//label[text()='Allen Solly'])[1]");
			builder1.moveToElement(AllenSolly).perform();
			AllenSolly.click();*/
			driver.findElementByXPath("//input[@value='Allen Solly']/following-sibling::div").click();
			driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']").click();
			/*builder1.moveToElement(closeButton).perform();
			closeButton.click();*/


			// Print Only Allen Solly Brand Minimum Price
			//

			// Find the costliest Jacket
			List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

			onlyPrice = new ArrayList<String>();

			for (WebElement productPrice : allenSollyPrices) {
				onlyPrice.add(productPrice.getText().replaceAll("\\D", ""));
			}

			// Get the minimum Price 
			String min = Collections.min(onlyPrice);

			// Find the lowest priced Allen Solly
			System.out.println("Minimum price is "+min);

		}

	}


