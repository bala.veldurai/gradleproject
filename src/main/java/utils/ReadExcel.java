package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getData(String Data) throws IOException {

		XSSFWorkbook wBook=new XSSFWorkbook("./excelData/"+Data+".xlsx");
		XSSFSheet sheet = wBook.getSheetAt(0);
		int lastRow = sheet.getLastRowNum();
		int colCount= sheet.getRow(0).getLastCellNum();
		System.out.println("total col count is "+colCount);
		System.out.println("Last Row count is "+lastRow);
		Object[][] datas=new Object[lastRow][colCount];
		for(int i=1;i<=lastRow;i++) {
			XSSFRow row = sheet.getRow(i);
			for(int j=0;j<colCount;j++) {
				datas[i-1][j] = row.getCell(j).getStringCellValue();

			}
		}	
		return datas;
	}
}
